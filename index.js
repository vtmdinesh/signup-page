let myFormEl = document.getElementById("myForm")
let firstNameEl = document.getElementById("firstName")
let lastNameEl = document.getElementById("lastName")
let emailEl = document.getElementById("email")
let passwordEl = document.getElementById("password")
let confirmPasswordEl = document.getElementById("confirmPassword")
let checkboxEl = document.getElementById("checkBox")
let buttonEl = document.getElementById("submitBtn")
let firstNameErrEl = document.getElementById("firstNameErr")
let lastNameErrEl = document.getElementById("lastNameErr")
let emailErrEl = document.getElementById("emailErr")
let pwdErrEl = document.getElementById("pwdErr")
let confirmPwdErrEl = document.getElementById("confirmPwdErr")
let checkBoxErrEl = document.getElementById("checkBoxErr")
let failureMessageEl = document.getElementById("message")
let eyeIconEl = document.getElementById("eyeIcon")
let successMsgEl = document.getElementById("successMsg")


let togglePassword = () => {

    document.getElementById("eye").classList.toggle("hide")

    document.getElementById("eyeSlash").classList.toggle("hide")
    console.log(passwordEl.type)
    if (passwordEl.type === "password") {
        passwordEl.type = "text"
    }
    else {
        passwordEl.type = "password"

    }
}



let passwordValidator = (password) => {
    let lowerCaseLetters = /[a-z]/g
    let upperCaseLetters = /[A-Z]/g
    let numbers = /[0-9]/g

    let upperCaseArr = password.match(upperCaseLetters)
    let lowerCaseArr = password.match(lowerCaseLetters)
    let numbersArr = password.match(numbers)
    let length = password.length

    if (upperCaseArr !== null && lowerCaseArr.length !== null && numbersArr !== null && length >= 8) {

        return true
    }
    else {

        return false
    }

}

let printPwdError = (isValid) => {

    if (isValid) {
        pwdErrEl.textContent = ""
    }
    else {
        pwdErrEl.textContent = "Password should contain alteast one uppercase,lowercase letter number and 8 characters long "
    }
}


let comparePasswords = (password, confirmPassword) => {

    if (confirmPassword === "") {
        confirmPwdErrEl.textContent = "Please re-enter password"
        return false
    }
    else if (password === confirmPassword) {
        confirmPwdErrEl.textContent = ""
        return true

    }
    else {
        confirmPwdErrEl.textContent = "Passwords does not match"
        return false
    }
}


let getCheckboxValidation = (isChecked) => {
    if (isChecked) {
        checkBoxErrEl.textContent = ""
        return true
    }
    else {
        checkBoxErrEl.textContent = "Please agree to terms and conditions"
        return false
    }
}

let nameValidation = (name) => {
    const regex = /^[a-zA-Z]+$/

    if (name.match(regex)) {
        return true
    }
    else {
        return false
    }
}

let printFirstNameErr = (result) => {
    if (result) {
        firstNameErrEl.textContent = ""
    }
    else {

        firstNameErrEl.textContent = `Please enter first name`
    }

}

let printLastNameErr = (result) => {
    if (result) {

        lastNameErrEl.textContent = ""

    }
    else {
        lastNameErrEl.textContent = `Please enter last name`

    }
}

let getEmailValidation = (email) => {

    let validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!email.match(validRegex)) {
        emailErrEl.textContent = "Please enter valid email address"
        return false
    }
    else {
        emailErrEl.textContent = ""
        return true
    }

}

let checkMatch = (passwordStatus, password, confirmPassword) => {
    if (passwordStatus) {
        pwdErrEl.textContent = ""
        return comparePasswords(password, confirmPassword);

    }

    else {
        pwdErrEl.textContent = "Password should contain alteast one uppercase,lowercase letter number and 8 characters long "
        return false
    }
}


const validateData = () => {
    let firstName = firstNameEl.value
    let lastName = lastNameEl.value
    let email = emailEl.value
    let password = passwordEl.value
    let confirmPassword = confirmPasswordEl.value
    let isChecked = checkboxEl.checked


    let isCheckboxValid = getCheckboxValidation(isChecked)
    let isFirstNameValid = nameValidation(firstName)
    printFirstNameErr(isFirstNameValid)
    let isLastNameValid = nameValidation(lastName)
    printLastNameErr(isLastNameValid)
    let isEmailValid = getEmailValidation(email)
    let passwordStatus = passwordValidator(password)
    printPwdError(passwordStatus)
    let isMatch = checkMatch(passwordStatus, password, confirmPassword)
    comparePasswords(password, confirmPassword)


    if (isCheckboxValid && isFirstNameValid && isLastNameValid && isEmailValid && isMatch) {
        failureMessageEl.classList.add("hide")
        successMsgEl.classList.remove("hide")
        firstNameEl.value = ""
        lastNameEl.value = ""
        emailEl.value = ""
        passwordEl.value = ""
        confirmPasswordEl.value = ""
        checkboxEl.checked = false
        buttonEl.disabled = true;
        myFormEl.classList.add("hide")

    }
    else {
        failureMessageEl.classList.remove("hide")
        successMsgEl.classList.add("hide")

    }

}



myFormEl.addEventListener("submit", (event) => {
    event.preventDefault();
    validateData()
})
eyeIconEl.addEventListener("click", togglePassword)
passwordEl.addEventListener("input", () => {
    password = passwordEl.value
    let isValid = passwordValidator(password)
    printPwdError(isValid)
})

confirmPasswordEl.addEventListener("input", () => {
    let password = passwordEl.value
    let confirmPassword = confirmPasswordEl.value
    let isValid = passwordValidator(confirmPassword)
    if (isValid) {
        comparePasswords(password, confirmPassword)
    }
    else {
        confirmPwdErrEl.textContent = "Password should contain alteast one uppercase,lowercase letter number and 8 characters long "
    }

})

checkboxEl.addEventListener("change", () => {
    let isChecked = checkboxEl.checked
    getCheckboxValidation(isChecked)
})

firstNameEl.addEventListener("blur", () => {
    let firstName = firstNameEl.value
    firstName = firstName.trim()
    firstNameEl.value = firstName
    let result = nameValidation(firstName)
    printFirstNameErr(result)

})


lastNameEl.addEventListener("blur", () => {
    let lastName = lastNameEl.value
    lastName = lastName.trim()
    lastNameEl.value = lastName
    let result = nameValidation(lastName)
    printLastNameErr(result)
})

myFormEl.addEventListener("click", () => {
    buttonEl.disabled = false
    successMsgEl.classList.add("hide")
})


emailEl.addEventListener("blur", () => {
    email = emailEl.value
    getEmailValidation(email)
})
